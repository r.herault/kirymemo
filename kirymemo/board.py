from random          import randint

from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ListProperty, BooleanProperty
from kivy.metrics    import dp
from kivy.parser     import parse_color
from kivy.clock      import Clock
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout

from .tile           import Tile

COLORS = ["#FFFFFF"]
ITEMS=[parse_color("#FF0000"), parse_color("#FFFF00"), parse_color("#00FF00"), parse_color("#00FFFF"), parse_color("#0000FF"), parse_color("#FF00FF"), parse_color("#ABCDEF"), parse_color("#FEDCBA")]
# Tiles par paires
SIZE_BOARD_X = 4
SIZE_BOARD_Y = 4

class Board(Widget):
    hmargin = NumericProperty(dp(10))
    vmargin = NumericProperty(dp(10))
    nrows   = NumericProperty(SIZE_BOARD_X)
    ncols   = NumericProperty(SIZE_BOARD_Y)
    tsize   = NumericProperty(0)
    tmargin = NumericProperty(dp(1))
    colors  = ListProperty([parse_color(c) for c in COLORS])
    tx      = NumericProperty(0)
    ty      = NumericProperty(0)
    list_items = ListProperty()
    tiles_activated = ListProperty([])
    game_mode = 2
    listeJ = ListProperty([{"id":0,"pseudo":"Joueur 1", "score":0}])
    idJActuel = 0

    #Popup de selection du gamemode
    bl = BoxLayout(orientation = "vertical")
    ipt = TextInput(text='2', multiline=False, size=(100, 100))
    bl.add_widget(ipt)
    label = Label(text="")
    bl.add_widget(label)
    popupGm = Popup(title='Entrez le nombre de case identique :', content=bl,
                size_hint=(None, None), size=(400, 200), auto_dismiss=False)

    #Popup de selection du nombre de joueurs
    blNbJ = BoxLayout(orientation = "vertical")
    iptNbJ = TextInput(text='2', multiline=False, size=(100, 100))
    blNbJ.add_widget(iptNbJ)
    labelNbJ = Label(text="")
    blNbJ.add_widget(labelNbJ)
    popupNbJ = Popup(title='Entrez le nombre de joueurs :', content=blNbJ,
                size_hint=(None, None), size=(400, 200), auto_dismiss=False)

    #Popup de selection du pseudo du joueur
    blPseudoJ = BoxLayout(orientation = "vertical")
    labelPseudoJ = Label(text="")
    blPseudoJ.add_widget(labelPseudoJ)
    iptPseudoJ = TextInput(text='', multiline=False, size=(100, 100))
    blPseudoJ.add_widget(iptPseudoJ)
    popupPseudoJ = Popup(title='Entrez le pseudo du joueur :', content=blPseudoJ,
                size_hint=(None, None), size=(400, 200), auto_dismiss=False)

    #Popup d'affichage du tour'
    blScore = BoxLayout(orientation = "vertical")
    btnT = Button(text='Ok')
    blScore.add_widget(btnT)
    blJ = BoxLayout(orientation = "vertical")
    blScore.add_widget(blJ)
    titleBase = "C'est au tour de "
    popupT = Popup(title="", content=blScore,
                size_hint=(None, None), size=(400, 300), auto_dismiss=False)

    #Popup de victoire
    btn = Button(text='Rejouer')
    blEnd = BoxLayout(orientation = "vertical")
    blEnd.add_widget(btn)
    blJEnd = BoxLayout(orientation = "vertical")
    blEnd.add_widget(blJEnd)
    titleBaseEnd = "Felicitations, "
    popup = Popup(title="", content=blEnd,
                size_hint=(None, None), size=(400, 300), auto_dismiss=False)

    #Popup de Menu
    blMenu = BoxLayout(orientation = "vertical")
    btnReset = Button(text = "Recommencer la partie")
    blMenu.add_widget(btnReset)
    btnGM = Button(text = "Choisir son mode de jeu")
    blMenu.add_widget(btnGM)
    btnJ = Button(text = "Choisir le nombre de joueurs")
    blMenu.add_widget(btnJ)
    popupMenu = Popup(title="Menu", content=blMenu,
                size_hint=(None, None), size=(400, 600), auto_dismiss=False)

    def __init__(self, **kwargs):
        super(Board, self).__init__(**kwargs)
        self.matrix = None
        self.fast   = False
        self.list_items = ITEMS*self.game_mode
        self.init()

    def init(self):
        if self.matrix:
            for r in self.matrix:
                for t in r:
                    self.remove_widget(t)
        self.matrix = []
        for i in range(self.nrows) :
            liste = []
            for j in range(self.ncols) :
                item = self.list_items.pop(randint(0, len(self.list_items)-1))
                liste.append(Tile(i, j, item, self))
            self.matrix.append(liste)
        for j in self.listeJ:
            j["score"] = 0

    def reset(self, *args):
        self.popupMenu.dismiss()
        self.tiles_activated = []
        self.__init__()

    def isFinished(self, *args):
        for rowTiles in self.matrix:
            for tile in rowTiles:
                if tile.rgba != parse_color("#000000"):
                    return False
        self.displayEnd()
        return True

    def displayMenu(self):
        self.btnReset.bind(on_press=self.reset)
        self.btnGM.bind(on_press=self.displaySelectGm)
        self.btnJ.bind(on_press=self.displayNbJ)
        self.popupMenu.open()

    def displayEnd(self):
        gagnant = max(self.listeJ, key=self.sortGagnant)
        self.popup.title = self.titleBaseEnd + gagnant["pseudo"] + " a remporté la partie avec " + str(gagnant["score"]) + " points !"
        self.blJEnd.clear_widgets()
        for j in self.listeJ:
            self.blJEnd.add_widget(Label(text = j["pseudo"] + " : " + str(j["score"])))
        self.btn.bind(on_press=self.initAndClosePopup)
        self.popup.open()

    def sortGagnant(self, item):
        return item["score"]

    def displaySelectGm(self, *args):
        self.popupMenu.dismiss()
        self.ipt.bind(on_text_validate=self.changeGameMode)
        self.label.text = ""
        self.popupGm.open()

    def displayNbJ(self, *args):
        self.popupMenu.dismiss()
        self.iptNbJ.bind(on_text_validate=self.changeNbJ)
        self.labelNbJ.text = ""
        self.popupNbJ.open()

    def initAndClosePopup(self, *args):
        self.__init__()
        self.popup.dismiss()

    def findNcolsNrows(self, gm):
        #8 correspond au nombre de couleurs de base dans list_items avant d'être multipliée par game_mode
        nbTiles = 8 * gm
        nrows = 4
        while nrows <= 10:
            for ncols in range(4, 10):
                if nrows * ncols == nbTiles:
                    return (nrows, ncols)
            nrows += 1
        self.label.text = "Le nombre saisi ne peut pas être traité"

    def changeGameMode(self, gm):
        if type(gm) != int:
            try:
                gm = int(gm.text)
            except ValueError:
                self.label.text = "Votre saisie n'est pas valide"
                return
        res = self.findNcolsNrows(gm)
        if res:
            self.game_mode = gm
            self.nrows = res[0]
            self.ncols = res[1]
            for rowTiles in self.matrix:
                for tile in rowTiles:
                    tile.rgba = parse_color("#000000")
            self.reset()
            self.popupGm.dismiss()

    def displayPseudo(self, indice):
        self.popupNbJ.dismiss()
        self.labelPseudoJ.text = "Joueur "+str(indice+1) + " : "
        self.iptPseudoJ.text = ""
        self.iptPseudoJ.bind(on_text_validate = self.changePseudo)
        self.popupPseudoJ.open()

    def changePseudo(self, pseudo):
        i = int(self.labelPseudoJ.text[-4])
        self.listeJ[i-1]["pseudo"] = pseudo.text
        self.popupPseudoJ.dismiss(animation = False)
        if i == len(self.listeJ):
            self.continueChangeNbJ()
        else:
            self.displayPseudo(i)

    def continueChangeNbJ(self):
        self.reset()
        self.blJ.clear_widgets()
        for j in self.listeJ:
            l = Label(text=j["pseudo"]+" : "+str(j["score"]))
            self.blJ.add_widget(l)
        self.popupT.title = self.titleBase + self.listeJ[self.idJActuel]["pseudo"]
        self.btnT.bind(on_press=self.displayChangeJ)
        self.popupT.open()

    def changeNbJ(self, nbJ):
        try:
            nbJ = int(nbJ.text)
            if nbJ == "0":
                self.labelNbJ.text = "Votre saisie n'est pas valide"
                return
            self.listeJ = []
            for i in range(0, nbJ):
                self.listeJ.append({"id":i,"pseudo":"Joueur "+str(i+1), "score":0})
            self.displayPseudo(0)
        except ValueError:
            self.labelNbJ.text = "Votre saisie n'est pas valide"
            return

    def displayChangeJ(self, *args):
        self.popupT.dismiss()

    def changeJActuel(self):
        if self.idJActuel+1 >= len(self.listeJ):
            self.idJActuel = 0
        else :
            self.idJActuel = self.idJActuel + 1
        self.blJ.clear_widgets()
        for j in self.listeJ:
            l = Label(text=j["pseudo"]+" : "+str(j["score"]))
            self.blJ.add_widget(l)
        self.popupT.title = self.titleBase + self.listeJ[self.idJActuel]["pseudo"]
        self.btnT.bind(on_press=self.displayChangeJ)
        if len(self.listeJ) != 1:
            self.popupT.open()

class Counter(object):

    def __init__(self, n):
        self.n = n

    def decr(self):
        self.n -= 1
