from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ObjectProperty, StringProperty
from kivy.animation  import Animation
from kivy.parser     import parse_color
import time

class Tile(Widget):

    board = ObjectProperty(None)
    row   = NumericProperty(0)
    col   = NumericProperty(0)
    hoff  = NumericProperty(0)
    voff  = NumericProperty(0)
    rgba  = ObjectProperty((1,1,1,1))
    rgba2 = ObjectProperty()

    def __init__(self, i, j, item, board):
        self.row = i
        self.col = j
        self.board = board
        super(Tile, self).__init__()
        self.rgba = board.colors[0]
        self.board.add_widget(self)
        self.rgba2 = item

    def on_touch_down(self, touch):
        #Au clique sur une case, on la retourne puis..
        if self.board.fast:
            return
        if self.collide_point(touch.x, touch.y):
            if self.rgba != parse_color("#000000"):
                self.flip()
            
            return True

    def flip(self):
        d = 0.5
        a  = Animation(rgba=self.rgba2, duration=d)
        a.bind(on_complete=self.test_tiles)
        a.start(self)

    def test_tiles(self, *args):
        if self in self.board.tiles_activated:
            #Si on clique sur une case déjà retournée
            return
        else:
            if len(self.board.tiles_activated) != self.board.game_mode-1:
                #Si on a pas retourné assez de case pour faire la vérif
                self.board.tiles_activated.append(self)
            else:
                if self.verifListe():
                    #Si les cases sont toutes de la meme couleurs, on les supprime et on donne un point au J actuel
                    self.delete()
                    for tile in self.board.tiles_activated:
                        tile.delete()
                    self.board.tiles_activated = []
                    self.board.listeJ[self.board.idJActuel]["score"] += 1
                else:
                    #Sinon, on les retourne et on passe le tour
                    self.unflip()
                    for tile in self.board.tiles_activated:
                        tile.unflip()
                    self.board.tiles_activated = []
                    self.board.changeJActuel()

    def verifListe(self):
        color = self.rgba2
        for tile in self.board.tiles_activated:
            if tile.rgba2 != self.rgba2:
                return False
        return True

    def unflip(self):
        d = 0.5
        a  = Animation(rgba=parse_color("#FFFFFF"), duration=d)
        a.start(self)

    def delete(self):
        d = 0.5
        a  = Animation(rgba=parse_color("#000000"), duration=d)
        #Test si toutes les cases ont été delete
        a.bind(on_complete=self.board.isFinished)
        a.start(self)